package ru.apolyakov.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void createTask();

    void clearTasks();

    void showTaskByIndex();

    void showTaskById();

    void showTaskByName();

    void removeTaskByIndex();

    void removeTaskById();

    void removeTaskByName();

    void updateTaskByIndex();

    void updateTaskById();

    void startTaskById();

    void startTaskByIndex();

    void startTaskByName();

    void finishTaskById();

    void finishTaskByIndex();

    void finishTaskByName();

    void findAllTaskByProjectId();

    void bindTaskByProjectId();

    void unbindTaskByProjectId();

    void removeAllTaskByProjectId();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

    void changeTaskStatusByName();

}

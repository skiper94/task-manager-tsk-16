package ru.apolyakov.tm.api.service;

import ru.apolyakov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}

package ru.apolyakov.tm.service;

import ru.apolyakov.tm.api.enumerated.Status;
import ru.apolyakov.tm.api.repository.ITaskRepository;
import ru.apolyakov.tm.api.service.ITaskService;
import ru.apolyakov.tm.exception.empty.EmptyDescriptionException;
import ru.apolyakov.tm.exception.empty.EmptyIdException;
import ru.apolyakov.tm.exception.empty.EmptyNameException;
import ru.apolyakov.tm.exception.entity.TaskNotFoundException;
import ru.apolyakov.tm.exception.system.ComparatorIncorrectException;
import ru.apolyakov.tm.exception.system.IndexIncorrectException;
import ru.apolyakov.tm.exception.system.StatusIncorrectException;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository){
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        if (comparator == null) throw new ComparatorIncorrectException();
        return taskRepository.findAll(comparator);
    }

    @Override
    public Task add(final String name, final String description){
        if(name == null || name.isEmpty()) throw new EmptyNameException();
        if(description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public void add(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.add(task);
    }

    @Override
    public void remove(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.remove(task);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task removeOneByName(final String name){
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeOneByName(name);
    }

    @Override
    public Task removeOneById(final String id){
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.removeOneById(id);
    }

    @Override
    public Task findOneById(final String id){
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findOneById(id);
    }

    @Override
    public Task findOneByIndex(final Integer index){
        if (index == null || index < 0) throw new IndexIncorrectException();
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task findOneByName(final String name){
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findOneByName(name);
    }

    @Override
    public Task updateTaskById(final String id, final String name, final String description){
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task removeTaskByIndex(final Integer index){
        if (index == null || index < 0) throw new IndexIncorrectException();
        return taskRepository.removeOneByIndex(index);
    }

    @Override
    public Task updateTaskByIndex(final Integer index, final String name, final String description){
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startTaskById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Task task = finishTaskByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = finishTaskByName(name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishTaskById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new StatusIncorrectException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status) {
        if (index == null) throw new IndexIncorrectException();
        if (status == null) throw new StatusIncorrectException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByName(final String name, final Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new StatusIncorrectException();
        final Task task = findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

}

package ru.apolyakov.tm.bootstrap;

import ru.apolyakov.tm.api.controller.ICommandController;
import ru.apolyakov.tm.api.controller.IProjectController;
import ru.apolyakov.tm.api.controller.ITaskController;
import ru.apolyakov.tm.api.repository.ICommandRepository;
import ru.apolyakov.tm.api.repository.IProjectRepository;
import ru.apolyakov.tm.api.repository.ITaskRepository;
import ru.apolyakov.tm.api.service.*;
import ru.apolyakov.tm.constant.ArgumentConst;
import ru.apolyakov.tm.constant.TerminalConst;
import ru.apolyakov.tm.controller.CommandController;
import ru.apolyakov.tm.controller.ProjectController;
import ru.apolyakov.tm.controller.TaskController;
import ru.apolyakov.tm.exception.system.UnknownArgumentException;
import ru.apolyakov.tm.exception.system.UnknownCommandException;
import ru.apolyakov.tm.repository.CommandRepository;
import ru.apolyakov.tm.repository.ProjectRepository;
import ru.apolyakov.tm.repository.TaskRepository;
import ru.apolyakov.tm.service.*;
import ru.apolyakov.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService, projectTaskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final ILoggerService loggerService = new LoggerService();

    public void run(final String... args){
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        if(parseArgs(args)) System.exit(0);
        while(true){
            try{
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                loggerService.command(command);
                parseCommand(command);
                System.err.println("[OK]");
            } catch (final Exception e){
                loggerService.error(e);
                System.err.println(e.getMessage());
            }
        }
    }

    public void parseArg(final String arg){
        if(arg == null) return;
        switch (arg){
            case ArgumentConst.ARG_ABOUT: commandController.showAbout(); break;
            case ArgumentConst.ARG_VERSION: commandController.showVersion(); break;
            case ArgumentConst.ARG_HELP: commandController.showHelp(); break;
            case ArgumentConst.ARG_INFO: commandController.showSystemInfo(); break;
            case ArgumentConst.ARG_CMD: commandController.showCommands(); break;
            case ArgumentConst.ARG_ARGUMENT: commandController.showArguments(); break;
            default: throw new UnknownArgumentException();
        }
    }

    public void parseCommand(final String command){
        if(command == null) throw new UnknownCommandException("Empty command");
        switch (command){
            case TerminalConst.CMD_ABOUT: commandController.showAbout(); break;
            case TerminalConst.CMD_VERSION: commandController.showVersion(); break;
            case TerminalConst.CMD_HELP: commandController.showHelp(); break;
            case TerminalConst.CMD_INFO: commandController.showSystemInfo(); break;
            case TerminalConst.CMD_ARGUMENTS: commandController.showArguments(); break;
            case TerminalConst.CMD_COMMANDS: commandController.showCommands(); break;
            case TerminalConst.CMD_TASK_LIST: taskController.showTasks(); break;
            case TerminalConst.CMD_TASK_CREATE: taskController.createTask(); break;
            case TerminalConst.CMD_TASK_CLEAR: taskController.clearTasks(); break;
            case TerminalConst.CMD_TASK_REMOVE_BY_ID: taskController.removeTaskById(); break;
            case TerminalConst.CMD_TASK_REMOVE_BY_INDEX: taskController.removeTaskByIndex(); break;
            case TerminalConst.CMD_TASK_REMOVE_BY_NAME: taskController.removeTaskByName(); break;
            case TerminalConst.CMD_TASK_VIEW_BY_ID: taskController.showTaskById(); break;
            case TerminalConst.CMD_TASK_VIEW_BY_INDEX: taskController.showTaskByIndex(); break;
            case TerminalConst.CMD_TASK_VIEW_BY_NAME: taskController.showTaskByName(); break;
            case TerminalConst.CMD_TASK_UPDATE_BY_ID: taskController.updateTaskById(); break;
            case TerminalConst.CMD_TASK_UPDATE_BY_INDEX: taskController.updateTaskByIndex(); break;
            case TerminalConst.CMD_TASK_START_BY_ID: taskController.startTaskById(); break;
            case TerminalConst.CMD_TASK_START_BY_INDEX: taskController.startTaskByIndex(); break;
            case TerminalConst.CMD_TASK_START_BY_NAME: taskController.startTaskByName(); break;
            case TerminalConst.CMD_TASK_FINISH_BY_ID: taskController.finishTaskById(); break;
            case TerminalConst.CMD_TASK_FINISH_BY_INDEX: taskController.finishTaskByIndex(); break;
            case TerminalConst.CMD_TASK_FINISH_BY_NAME: taskController.finishTaskByName(); break;
            case TerminalConst.CMD_TASK_VIEW_BY_PROJECT_ID: taskController.findAllTaskByProjectId(); break;
            case TerminalConst.CMD_TASK_BINDING_TO_PROJECT: taskController.bindTaskByProjectId(); break;
            case TerminalConst.CMD_TASK_UNBINDING_FROM_PROJECT: taskController.unbindTaskByProjectId(); break;
            case TerminalConst.CMD_TASK_ALL_REMOVE_FROM_PROJECT: taskController.removeAllTaskByProjectId(); break;
            case TerminalConst.CMD_TASK_CHANGE_STATUS_BY_ID: taskController.changeTaskStatusById(); break;
            case TerminalConst.CMD_TASK_CHANGE_STATUS_BY_INDEX: taskController.changeTaskStatusByIndex(); break;
            case TerminalConst.CMD_TASK_CHANGE_STATUS_BY_NAME: taskController.changeTaskStatusByName(); break;
            case TerminalConst.CMD_PROJECT_REMOVE_BY_ID: projectController.removeProjectById(); break;
            case TerminalConst.CMD_PROJECT_REMOVE_BY_INDEX: projectController.removeProjectByIndex(); break;
            case TerminalConst.CMD_PROJECT_REMOVE_BY_NAME: projectController.removeProjectByName(); break;
            case TerminalConst.CMD_PROJECT_VIEW_BY_ID: projectController.showProjectById(); break;
            case TerminalConst.CMD_PROJECT_VIEW_BY_INDEX: projectController.showProjectByIndex(); break;
            case TerminalConst.CMD_PROJECT_VIEW_BY_NAME: projectController.showProjectByName(); break;
            case TerminalConst.CMD_PROJECT_UPDATE_BY_ID: projectController.updateProjectById(); break;
            case TerminalConst.CMD_PROJECT_UPDATE_BY_INDEX: projectController.updateProjectByIndex(); break;
            case TerminalConst.CMD_PROJECT_LIST: projectController.showProjects(); break;
            case TerminalConst.CMD_PROJECT_CREATE: projectController.createProject(); break;
            case TerminalConst.CMD_PROJECT_CLEAR: projectController.clearProjects(); break;
            case TerminalConst.CMD_PROJECT_START_BY_ID: projectController.startProjectById(); break;
            case TerminalConst.CMD_PROJECT_START_BY_INDEX: projectController.startProjectByIndex(); break;
            case TerminalConst.CMD_PROJECT_START_BY_NAME: projectController.startProjectByName(); break;
            case TerminalConst.CMD_PROJECT_FINISH_BY_ID: projectController.finishProjectById(); break;
            case TerminalConst.CMD_PROJECT_FINISH_BY_INDEX: projectController.finishProjectByIndex(); break;
            case TerminalConst.CMD_PROJECT_FINISH_BY_NAME: projectController.finishProjectByName(); break;
            case TerminalConst.CMD_PROJECT_REMOVE_BY_ID_WITH_CHILDREN: projectController.removeProjectByIdWithChildren(); break;
            case TerminalConst.CMD_PROJECT_CHANGE_STATUS_BY_ID: projectController.changeProjectStatusById(); break;
            case TerminalConst.CMD_PROJECT_CHANGE_STATUS_BY_INDEX: projectController.changeProjectStatusByIndex(); break;
            case TerminalConst.CMD_PROJECT_CHANGE_STATUS_BY_NAME: projectController.changeProjectStatusByName(); break;
            case TerminalConst.CMD_EXIT: commandController.exit(); break;
            default: throw new UnknownCommandException(command);
        }
    }

    public boolean parseArgs(final String[] args){
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

}
